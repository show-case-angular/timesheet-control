import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { TimesheetEntry } from 'src/app/core/model/timesheet-entry';
import * as moment from 'moment';

@Component({
  selector: 'app-timesheet-item',
  templateUrl: './timesheet-item.component.html',
  styleUrls: ['./timesheet-item.component.scss'],
})
export class TimesheetItemComponent {
  @Input() entry: TimesheetEntry = {} as TimesheetEntry;
  totalTime: string;

  constructor() { }

}
