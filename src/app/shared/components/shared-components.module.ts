import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TheraButtonComponent } from './thera-button/thera-button.component';
import { IonicModule } from '@ionic/angular';
import { TimesheetItemComponent } from './timesheet-item/timesheet-item.component';
import { TimesheetItemEntryComponent } from './timesheet-item-entry/timesheet-item-entry.component';
import { PipesModule } from '../pipes/pipes.module';



@NgModule({
  declarations: [
    TheraButtonComponent,
    TimesheetItemComponent,
    TimesheetItemEntryComponent
  ],
  imports: [
    CommonModule,
    IonicModule.forRoot(),
    PipesModule
  ],
  exports: [
    TheraButtonComponent,
    TimesheetItemComponent,
    TimesheetItemEntryComponent
  ]
})
export class SharedComponentsModule { }
