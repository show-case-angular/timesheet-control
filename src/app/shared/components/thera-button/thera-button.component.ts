import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-thera-button',
  templateUrl: './thera-button.component.html',
  styleUrls: ['./thera-button.component.scss'],
})
export class TheraButtonComponent implements OnInit {
  @Input() title: string;
  @Output() buttonClicked = new EventEmitter();
  @Input() active: boolean;
  @Input() disabled: boolean;

  constructor() { }

  ngOnInit() {}

  onButtonClicked() {
    this.buttonClicked.emit();
  }

}
