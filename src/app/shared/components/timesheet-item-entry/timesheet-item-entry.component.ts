import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-timesheet-item-entry',
  templateUrl: './timesheet-item-entry.component.html',
  styleUrls: ['./timesheet-item-entry.component.scss'],
})
export class TimesheetItemEntryComponent implements OnInit {
  @Input() title: string;
  @Input() val: string;

  constructor() { }

  ngOnInit() {}

}
