import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'formatTimeEntry'
})
export class FormatTimeEntryPipe implements PipeTransform {

  transform(value: string, format?: string): string {
    if (!value) {
      return '';
    }

    if (format) {
      if (
        value.indexOf(moment().format('YYYY-MM-DD')) > -1 &&
        Number(moment(value).utc().format('DDMMYYYYHHmmss')) > Number(moment().format('DDMMYYYYHHmmss'))
      ) {
        return moment(value).local().format(format);
      } else {
        return moment(value).utc().format(format);
      }
    }
    return moment(value).format('DD/MM/YYYY HH:mm:ss');
  }

}
