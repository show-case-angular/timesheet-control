import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatTimeEntryPipe } from './format-time-entry.pipe';



@NgModule({
  declarations: [
    FormatTimeEntryPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    FormatTimeEntryPipe
  ]
})
export class PipesModule { }
