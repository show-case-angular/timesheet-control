import * as moment from 'moment';

export class TimesheetEntry {
  public id: number;
  public start: string;
  public startLunch: string;
  public endLunch: string;
  public end: string;

  constructor(timeEntry?: TimesheetEntry) {
    this.id = timeEntry?.id;
    this.start = timeEntry?.start;
    this.startLunch = timeEntry?.startLunch;
    this.endLunch = timeEntry?.endLunch;
    this.end = timeEntry?.end;
  }

  get localStart() {
    if (
      this.start.indexOf(moment().format('YYYY-MM-DD')) > -1 &&
      Number(moment(this.start).utc().format('DDMMYYYYHHmmss')) > Number(moment().format('DDMMYYYYHHmmss'))
    ) {
      return moment(this.start).local();
    } else {
      return moment(this.start).utc();
    }
  }

  get hasStart() {
    return Boolean(this.start);
  }

  get hasEnd() {
    return Boolean(this.end);
  }

  get hasStartLunch() {
    return Boolean(this.startLunch);
  }

  get hasEndLunch() {
    return Boolean(this.endLunch);
  }

  get totalTime() {
    if (this.hasStart) {
      const end = moment(this.end).utc();
      const endLunch = moment(this.endLunch).utc();
      const startLuch = moment(this.startLunch).utc();

      if (this.hasStartLunch && this.hasEndLunch && this.hasEnd) {
        let workTime = moment(end.format('DD/MM/YYYY HH:mm:ss')).diff(moment(this.localStart.format('DD/MM/YYYY HH:mm:ss')), 'seconds');
        if (workTime < 0) {
          workTime = workTime * -1;
        }
        const lunchTime = endLunch.diff(startLuch, 'seconds');
        return moment.utc((workTime - lunchTime)*1000).format('HH:mm:ss');
      } else if(!this.hasStartLunch && !this.hasEndLunch && this.hasEnd) {
        const workTime = end.diff(this.localStart, 'seconds');
        return moment.utc((workTime)*1000).format('HH:mm:ss');
      } else if(!this.hasEnd && this.hasStartLunch && !this.hasEndLunch) {
        let workTime = moment(startLuch
          .format('DD/MM/YYYY HH:mm:ss'))
          .diff(moment(this.localStart.format('DD/MM/YYYY HH:mm:ss')), 'seconds');
        if (workTime < 0) {
          workTime = workTime * -1;
        }
        return moment.utc((workTime)*1000).format('HH:mm:ss');
      } else if(!this.hasEnd && this.hasStartLunch && this.hasEndLunch) {
        const workTime = moment(moment()).diff(moment(this.localStart.format('DD/MM/YYYY HH:mm:ss')), 'seconds');
        const lunchTime = endLunch.diff(startLuch, 'seconds');
        return moment.utc((workTime - lunchTime)*1000).format('HH:mm:ss');
      } else if(!this.hasStartLunch && !this.hasEndLunch && !this.hasEnd) {
        const workTime = moment().diff(this.localStart, 'seconds');
        return moment.utc((workTime)*1000).format('HH:mm:ss');
      }
    }
  }
}
