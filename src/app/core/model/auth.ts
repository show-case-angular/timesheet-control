export interface AuthInfo {
  accessToken: string;
  refreshToken: string;
  tokenType: string;
  expiresIn: number;
  name: string;
}
