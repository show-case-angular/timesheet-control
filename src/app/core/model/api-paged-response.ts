export interface ApiPagedResponse<T> {
  items: T[];
  count: number;
  currentPage: number;
  pageSize: number;
  totalPages: number;
  nextPage: string;
  previousPage: string;
}
