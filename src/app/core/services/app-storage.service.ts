import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { NativeStorage } from '@awesome-cordova-plugins/native-storage/ngx';

@Injectable({
  providedIn: 'root'
})
export class AppStorageService {
  isMobile: boolean;

  constructor(
    platform: Platform,
    private nativeStorage: NativeStorage
  ) {
    this.isMobile = false;
  }

  setItem(key: string, value: any): Promise<any> {
    if (this.isMobile) {
      return this.nativeStorage.setItem(key, JSON.stringify(value));
    }

    return new Promise((resolve, reject) => {
      localStorage.setItem(key, JSON.stringify(value));
      resolve(true);
    });
  }

  getItem<T>(key: string): Promise<T | null> {
    return new Promise(async (resolve, reject) => {
      try {
        let data: string;
        if (this.isMobile) {
          data = await this.nativeStorage.getItem(key);
        } else {
          data = localStorage.getItem(key);
        }
        if (!data) {
          resolve(null);
        }
        resolve(JSON.parse(data) as T);
      } catch (error) {
        console.log(error);
        resolve(null);
      }
    });
  }

  clear(): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      if (this.isMobile) {
        await this.nativeStorage.clear();
      } else {
        localStorage.clear();
      }
      resolve(true);
    });
  }
}
