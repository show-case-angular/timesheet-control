import { Injectable } from '@angular/core';
import { AppStorageService } from './app-storage.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthInfo } from '../model/auth';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private token: string;

  constructor(
    private appStorage: AppStorageService,
    private http: HttpClient
  ) {
    this.getAuthInfo().then(res => {
      this.token = res.accessToken;
    });
  }

  get authToken() {
    return this.token;
  }

  isLogged(): Promise<boolean> {
    console.log('checking login...');
    return new Promise(async (resolve, reject) => {
      this.appStorage.getItem<string>('auth-info').then(res => {
        if (!res) {
          resolve(false);
          return false;
        }
        resolve(true);
        return true;
      }).catch(error => {
        console.log(JSON.stringify(error));
        resolve(false);
      });
    });
  }

  logIn(username: string, password: string) {
    return new Promise((resolve, reject) => {
      try {
        this.http.post<AuthInfo>(`${environment.apiBaseUrl}/Accounts`, {
          userID: username,
          accessKey: password,
          grantType: 'password'
        }).subscribe(res => {
          this.appStorage.setItem('auth-info', res);
          this.token = res.accessToken;
          resolve(true);
        }, error => {
          console.log(error);
          resolve(false);
        });
      } catch (error) {
        resolve(false);
      }
    });
  }

  logout(): Promise<boolean> {
    return this.appStorage.clear();
  }

  getAuthInfo(): Promise<AuthInfo> {
    return this.appStorage.getItem<AuthInfo>('auth-info');
  }
}
