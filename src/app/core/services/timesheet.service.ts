import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ApiPagedResponse } from '../model/api-paged-response';
import { TimesheetEntry } from '../model/timesheet-entry';

@Injectable({
  providedIn: 'root'
})
export class TimesheetService {

  constructor(
    private http: HttpClient
  ) { }

  getTimesheetHistory(): Promise<ApiPagedResponse<TimesheetEntry>> {
    return new Promise((resolve, reject) => {
      this.http.get<ApiPagedResponse<TimesheetEntry>>(`${environment.apiBaseUrl}/Timesheet`).subscribe(res => {
        resolve(res);
      });
    });
  }

  createTimesheetEntry(timeEntry: Partial<TimesheetEntry>) {
    return new Promise((resolve, reject) => {
      this.http.post<TimesheetEntry>(`${environment.apiBaseUrl}/Timesheet`, {
        start: timeEntry.start
      }).subscribe(res => {
        resolve(res);
      });
    });
  }

  updateTimesheetEntry(timeEntry: Partial<TimesheetEntry>) {
    return new Promise((resolve, reject) => {
      this.http.put<TimesheetEntry>(`${environment.apiBaseUrl}/Timesheet/${timeEntry.id}`, timeEntry).subscribe(res => {
        resolve(res);
      });
    });
  }
}
