import { Injectable } from '@angular/core';
import { LoadingController, LoadingOptions } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  constructor(public loadingController: LoadingController) {}

  async presentLoading(message?: string, duration?: number) {
    const options: Partial<LoadingOptions> = {
      cssClass: 'my-custom-class',
      message: message || 'Please wait...'
    };

    if (duration) {
      options.duration = duration;
    }

    const loading = await this.loadingController.create(options);
    return loading;
  }
}
