import { Injectable } from '@angular/core';
import { AlertController, AlertOptions } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(
    private alertCtrl: AlertController
  ) { }

  async showAlert(options?: AlertOptions) {
    const alert = await this.alertCtrl.create(options);
    alert.present();
  }
}
