import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { LoginService } from '../services/login.service';
import { NavController } from '@ionic/angular';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private loginService: LoginService,
    private navCtrl: NavController
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    let handled = false;

    let authReq = req.clone({
      headers: req.headers
    });

    if (this.loginService.authToken) {
      authReq = req.clone({
        headers: authReq.headers.set('Authorization', `Bearer ${this.loginService.authToken}`)
      });
    }

    return next.handle(authReq).pipe(
      catchError((returnedError) => {
        let errorMessage = null;
        if (returnedError.error instanceof ErrorEvent) {
          errorMessage = `Error: ${returnedError.error.message}`;
        } else if (returnedError instanceof HttpErrorResponse) {
          handled = this.handleServerSideError(returnedError);
        }

        if (!handled) {
          if (errorMessage) {
            return throwError(errorMessage);
          } else {
            return throwError('Unexpected problem occurred');
          }
        } else {
          return of(returnedError);
        }
      })
    );
  }

  private handleServerSideError(error: HttpErrorResponse): boolean {
    const handled = false;

    switch (error.status) {
      case 401:
      case 403:
        this.loginService.logout();
        this.navCtrl.navigateRoot('/login');
        break;
    }

    return handled;
  }
}
