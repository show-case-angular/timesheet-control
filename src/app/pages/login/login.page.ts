import { Component, OnInit } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { LoginService } from 'src/app/core/services/login.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MessageService } from 'src/app/core/services/message.service';
import { LoadingService } from 'src/app/core/services/loading.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  isSubmitted: boolean;
  ionicForm: FormGroup;
  loading: HTMLIonLoadingElement;

  constructor(
    private loginService: LoginService,
    private navCtrl: NavController,
    public formBuilder: FormBuilder,
    private messageService: MessageService,
    private loadingService: LoadingService
  ) { }

  get errorControl() {
    return this.ionicForm.controls;
  }

  ngOnInit() {
    this.ionicForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });

    this.loadingService.presentLoading('Carregando...').then(res => {
      this.loading = res;
    });
  }

  async login() {
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log('Please provide all the required values!');
      return false;
    }
    this.loading.present();
    const isLoginValid = await this.loginService.logIn(this.ionicForm.value.email, this.ionicForm.value.password);
    this.loading.dismiss();
    if (!isLoginValid) {
      this.messageService.showAlert({
        message: 'Verifique os dados de acesso e tente novamente.',
        buttons: ['OK'],
        header: 'Usuário ou senha inválidos.'
      });
      return;
    }

    this.navCtrl.navigateRoot('/home');
  }

}
