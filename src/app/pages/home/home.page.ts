import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ApiPagedResponse } from 'src/app/core/model/api-paged-response';
import { TimesheetEntry } from 'src/app/core/model/timesheet-entry';
import { LoadingService } from 'src/app/core/services/loading.service';
import { LoginService } from 'src/app/core/services/login.service';
import { TimesheetService } from 'src/app/core/services/timesheet.service';
import * as moment from 'moment';

enum TimeType {
  lunchStart,
  lunchEnd,
  end
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {
  timeEntryType = {
    lunchStart: TimeType.lunchStart,
    lunchEnd: TimeType.lunchEnd,
    end: TimeType.end
  };
  entry: TimesheetEntry = new TimesheetEntry(null);
  timeHistory: ApiPagedResponse<TimesheetEntry>;
  loading: HTMLIonLoadingElement;
  username: string;
  now: Date;
  totalTime: string;
  interval;
  updatingEntry: boolean;

  constructor(
    private loginService: LoginService,
    private navCtrl: NavController,
    private timesheetService: TimesheetService,
    private loadingService: LoadingService
  ) {}

  ngOnDestroy(): void {
    clearInterval(this.interval);
  }

  ngOnInit(): void {
    this.loginService.getAuthInfo().then(res => {
      this.username = res.name;
    });

    this.loadingService.presentLoading('Carregando...').then(res => {
      this.loading = res;
      this.loadItems();
    });
  }

  updateTotalAndTimer() {
    this.totalTime = this.entry.totalTime;
    this.now = moment().utc().toDate();
  }

  loadItems() {
    this.loading.present();
    this.timesheetService.getTimesheetHistory().then(historyRes => {
      if (historyRes && historyRes.items.length) {
        const currentEntry = historyRes.items.find(t => t?.start.indexOf(moment().format('YYYY-MM-DD')) > -1);
        if (currentEntry) {
          this.entry = new TimesheetEntry(currentEntry);
          this.timeHistory = historyRes;
          this.timeHistory.items = historyRes.items.map(t => new TimesheetEntry(t)).filter(i => i.id !== currentEntry.id);
        } else {
          this.timeHistory = historyRes;
        }
      }
      this.loading.dismiss();
      this.updatingEntry = false;
      if (this.entry && this.entry.hasStart) {
        this.updateTotalAndTimer();
        if (!this.entry.hasEnd) {
          this.interval = setInterval(() => {
            this.updateTotalAndTimer();
          }, 1000);
        }
      }
    }).catch(err => {
      this.loading.dismiss();
    });
  }

  createTimeEntry() {
    this.updatingEntry = true;
    const currentDateTime = moment().format('YYYY-MM-DDTHH:mm:ss');
    this.loading.present();
    this.timesheetService.createTimesheetEntry({
      start: currentDateTime
    }).then(res => {
      this.loadItems();
    });
  }

  updateTimeEntry(timeType: TimeType) {
    this.loading.present();
    const entryUpdate: Partial<TimesheetEntry> = {
      id: this.entry.id
    };
    const currentDateTime = moment().format('YYYY-MM-DD HH:mm:ss');
    switch (timeType) {
      case TimeType.lunchStart:
        entryUpdate.startLunch = currentDateTime;
        break;
      case TimeType.lunchEnd:
        entryUpdate.endLunch = currentDateTime;
        break;
      case TimeType.end:
        entryUpdate.end = currentDateTime;
        break;
      default:
        break;
    }
    this.loading.present();
    this.timesheetService.updateTimesheetEntry(entryUpdate).then(res => {
      this.loadItems();
    });
  }

  async logout() {
    await this.loginService.logout();
    this.navCtrl.navigateRoot('/login');
  }

}
